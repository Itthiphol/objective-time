import React, { Component } from 'react';
import Layout from './components/Layout/Layout';
import Timer from './containers/Timer/Timer';

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Timer ></Timer>
        </Layout>
      </div>
    );
  }
}

export default App;