import React from 'react';
import Aux from '../../../hoc/Aux/Aux';

import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

/** ICONS **/
import PagesIcon from '@material-ui/icons/Pages';
import BookmarkIcon from '@material-ui/icons/Bookmarks';
import FolderOpenIcon from '@material-ui/icons/FolderOpen';
import HelpIcon from '@material-ui/icons/Help';
import HourglassFullIcon from '@material-ui/icons/HourglassFull';
import PollIcon from '@material-ui/icons/Poll';
import AssignmentIcon from '@material-ui/icons/Assignment';

import { makeStyles, useTheme } from '@material-ui/core/styles';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({

  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
}));

function sideDrawer(props) {
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>

            <ListItem button>
                <ListItemIcon>
                    <HourglassFullIcon />
                </ListItemIcon>
                <ListItemText
                    primary="Timer"
                />
            </ListItem>

            <ListItem button>
                <ListItemIcon>
                    <PollIcon />
                </ListItemIcon>
                <ListItemText
                    primary="Dashboard"
                />
            </ListItem>

            <ListItem button>
                <ListItemIcon>
                    <AssignmentIcon />
                </ListItemIcon>
                <ListItemText
                    primary="Reports"
                />
            </ListItem>

            <ListItem button>
                <ListItemIcon>
                    <PagesIcon />
                </ListItemIcon>
                <ListItemText
                    primary="Insights"
                />
            </ListItem>

      </List>
      <Divider />
      <List>
            <ListItem button>
                <ListItemIcon>
                    <FolderOpenIcon />
                </ListItemIcon>
                <ListItemText
                    primary="Categories"
                />
            </ListItem>
            <ListItem button>
                <ListItemIcon>
                    <BookmarkIcon />
                </ListItemIcon>
                <ListItemText
                    primary="Tags"
                />
            </ListItem>
            <ListItem button>
                <ListItemIcon>
                    <HelpIcon />
                </ListItemIcon>
                <ListItemText
                    primary="Help"
                />
            </ListItem>
      </List>
    </div>
  );

  return (
    <Aux>

        <nav className={classes.drawer} aria-label="mailbox folders">
            {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
            <Hidden smUp implementation="css">
            <Drawer
                container={container}
                variant="temporary"
                anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                open={ props.drawerState }
                onClose={ props.drawerToggleClicked }
                classes={{
                paper: classes.drawerPaper,
                }}
                ModalProps={{
                keepMounted: true, // Better open performance on mobile.
                }}
            >
                {drawer}
            </Drawer>
            </Hidden>
            <Hidden xsDown implementation="css">
            <Drawer
                classes={{
                paper: classes.drawerPaper,
                }}
                variant="permanent"
                open
            >
                {drawer}
            </Drawer>
            </Hidden>
        </nav>

    </Aux>

  );
}

export default sideDrawer;
