import React, {Component} from 'react';

import Aux from '../../../hoc/Aux/Aux';


const description = (props) => {
    return (
            <Aux>
                <h3>Description</h3>
              <input type="text" placeholder="What are you working on?" />
            </Aux>
        );
};

export default description;