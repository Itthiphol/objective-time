import React from "react";

import Aux from "../../hoc/Aux/Aux";
import Description from "./Description/Description";
import Moment from "react-moment";

import { Button } from "@material-ui/core";
import { TextField } from "@material-ui/core";

const TimerInsert = (props) => {

  const { timerTime } = props.timer;
  let centiseconds = ("0" + (Math.floor(timerTime / 10) % 100)).slice(-2);
  let seconds = ("0" + (Math.floor(timerTime / 1000) % 60)).slice(-2);
  let minutes = ("0" + (Math.floor(timerTime / 60000) % 60)).slice(-2);
  let hours = ("0" + Math.floor(timerTime / 3600000)).slice(-2);

  return (
    <Aux>
      <TextField
        id="outlined-uncontrolled"
        label="Description"
        placeholder="What are you working on?"
        value={ props.desc }
        margin="normal"
        variant="outlined"
        onChange={ props.changed }
        error={ props.descError }
      />

      <div>
        {hours} : {minutes} : {seconds} : {centiseconds}
      </div>

      {props.timer.timerOn === false && props.timer.timerTime === 0 && (
        <Button variant="contained" color="primary" onClick={props.started}>
          Start
        </Button>
      )}
      {props.timer.timerOn === true && (
        <Button variant="contained" color="primary" onClick={props.paused}>
          Pause
        </Button>
      )}
      {props.timer.timerOn === false && props.timer.timerTime > 0 && (
        <Button variant="contained" color="primary" onClick={props.started}>
          Resume
        </Button>
      )}

      { props.timer.timerTime ? (
        <Button variant="contained" color="primary" onClick={props.stopped} disabled={props.doneDisabled}>
          Done
        </Button>
      ) : null }

      <Description />

      <Moment format="YYYY/MM/DD">1976-04-19T12:59-0500</Moment>
    </Aux>
  );
};

export default TimerInsert;
