import React, {Component} from 'react';

import Aux from '../../hoc/Aux/Aux';
import TimerInsert from '../../components/TimerInsert/TimerInsert';

class Timer extends Component {

    state = {
        timerOn: false,
        timerStart: 0,
        timerTime: 0,
        description: '',
        categories: [],
        tags: [],
        doneBtnDisable: true,
        descInputErr: true
    };

    startTimer = () => {
        this.setState({
            timerOn: true,
            timerTime: this.state.timerTime,
            timerStart: Date.now() - this.state.timerTime
        });
        this.timer = setInterval(() => {
            this.setState({
                timerTime: Date.now() - this.state.timerStart
            });
        }, 10);

        if (this.state.description !== "") {
            this.setState({ doneBtnDisable:false })
        }
    };

    pauseTimer = () => {
        this.setState({
            timerOn: false
        });
        clearInterval(this.timer);
    };

    reset = (event) => {
        const defaultState = {
            timerOn: false,
            timerStart: 0,
            timerTime: 0,
            description: '',
            categories: [],
            tags: [],
            doneBtnDisable: true,
            descInputErr: true
        }
        this.setState({...defaultState})

        event.target.value = "";
        clearInterval(this.timer);
        console.log('reset');
    }

    descriptionChangedHandler = (event) => {
        this.setState({
            description: event.target.value,
        });
        if ( event.target.value !== '') {
            this.setState({
                descInputErr: false
            })
        } else {
            this.setState({
                descInputErr: true
            })
        }
        if (this.state.timerStart > 0) { this.setState({ doneBtnDisable: false }); }
    }


    render () {

        return (
          <Aux>
            <TimerInsert
                stopped={ this.reset }
                paused={ this.pauseTimer }
                started={ this.startTimer }
                timer={ this.state }
                changed={ this.descriptionChangedHandler }
                doneDisabled={ this.state.doneBtnDisable }
                descError={ this.state.descInputErr }
                desc={ this.state.description }
            />
            <h1>Hello world</h1>
            <h2>{this.state.description}</h2>
            <div>Timer List</div>
          </Aux>
        );
    }
}

export default Timer;